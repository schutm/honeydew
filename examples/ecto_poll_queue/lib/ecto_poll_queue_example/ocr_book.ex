# "Optical Character Recognition"

defmodule Queue.Poll.EctoExample.OCRBook do
  alias Queue.Poll.EctoExample.Repo
  alias Queue.Poll.EctoExample.Book

  def run(primary_keys) do
    book = Repo.get_by(Book, primary_keys)

    if book.from do
      send(book.from, {:ocr_job_ran, primary_keys})
    end

    if book.should_fail do
      raise "ocr's totally busted dude!"
    end
  end
end
