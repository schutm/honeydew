use Mix.Config

config :ecto_poll_queue_example, ecto_repos: [Queue.Poll.EctoExample.Repo]
config :ecto_poll_queue_example, interval: 0.5

config :logger,
  compile_time_purge_matching: [
    [level_lower_than: :warning]
  ],
  console: [level: :warning]

import_config "#{Mix.env()}.exs"
