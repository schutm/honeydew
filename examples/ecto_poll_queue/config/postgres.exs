use Mix.Config

config :ecto_poll_queue_example, Queue.Poll.EctoExample.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "honeydew_test",
  username: "postgres",
  password: "",
  hostname: "localhost"
